const mongoose = require('mongoose');

var ordersSchema = mongoose.Schema({
    order_name: {
        type: String,
        required: true
    },
    order_resto_id: {
        type: String,
        required: true
    },
    order_client_number: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {
        type: Date,
        default: Date.now
    }
});


const Orders = module.exports = mongoose.model('orders', ordersSchema);
module.exports.get = function (callback, limit) {
    Orders.find(callback).limit(limit);
}