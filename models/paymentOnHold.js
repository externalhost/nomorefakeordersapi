const mongoose = require('mongoose');

var payementSchema = mongoose.Schema({
    order_id: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {
        type: Date,
        default: Date.now
    }
});


const PayementOnHold = module.exports = mongoose.model('payement', payementSchema);
module.exports.get = function (callback, limit) {
    PayementOnHold.find(callback).limit(limit);
}