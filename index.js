let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let app = express();


let apiRoutes = require("./api-routes");

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

mongoose.connect('mongodb+srv://rooty:rootybrown1@nomorefakeorders.qhzfn.mongodb.net/nomorefakeorders?retryWrites=true&w=majority', { useNewUrlParser: true});
var db = mongoose.connection;
// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;
if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")

app.get('/', (req, res) => res.send('Hey ? what\'s did you expect ?'));

app.use('/api', apiRoutes);

app.listen(port, function () {
    console.log("Running RestHub on "+ port);
});