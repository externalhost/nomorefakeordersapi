let router = require('express').Router();
let auth = require('./controllers/auth');

router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'API Its Working',
    });
});

const userController = require('./controllers/user');
const ordersController = require('./controllers/orders');

router.route('/login')
    .post(userController.login);

router.route('/register')
    .post(userController.new);


router.route('/orders')
    .post(ordersController.new);

router.route('/orders/:id')
    .get(ordersController.index)


router.route('/order/:id')
    .post(ordersController.update)
    .get(ordersController.order)

module.exports = router;