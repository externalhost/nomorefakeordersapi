const Orders = require('../models/orders');
const PayementOnHold = require('../models/paymentOnHold');
const Vonage = require('@vonage/server-sdk')

const vonage = new Vonage({
  apiKey: "22bad07f",
  apiSecret: "Icjgh6NhmirG6emx"
})

exports.new = function (req, res) {
    let order = new Orders();
    let newPayement = new PayementOnHold();

    order.order_name = req.body.order_name;
    order.order_client_number = req.body.order_client_number;
    order.price = req.body.price;
    order.status = "NoPayed";
    order.order_resto_id = req.body.order_resto_id;
    
    order.save(function (err) {
        if (err) {
            res.json(err);
        } else {
            newPayement.order_id = order._id
            newPayement.status = "NoPayed";
            newPayement.save(function (err) {
                if (err) {
                    res.json(err)
                } else {
                    const message = `Pour finaliser votre commande de ${order.order_name} il faut vous aquitter de la somme de ${order.price} € en cliquant ici : https://nomorefakeordersfront.herokuapp.com/payement/${order._id}`;
                    vonage.message.sendSms("NoMoreFakeOrders", order.order_client_number, message, (err, responseData) => {
                        if (err) {
                            console.log(err);
                        } else {
                            if(responseData.messages[0]['status'] === "0") {
                                console.log("Message sent successfully.");
                                res.json({
                                    message: 'New order created!',
                                    data: order
                                });
                            } else {
                                res.json({
                                    message: 'New order created! but sms could not be sent',
                                    data: order
                                });
                                console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`);
                            }
                        }
                    })
                }
            })
            
        }
    });
};

exports.update = function (req, res) {
    Orders.findById(req.params.id, function (err, order) {
        if (err) {
            res.send(err);
        } else {
            order.status = "Payed";
            order.save(function (err) {
            if (err) {
                res.json(err);
            } else {
                res.json({
                    message: 'User Info updated',
                    data: order
                });
            }
            });
        }
    });
};

exports.order = function (req, res) {
    Orders.findById(req.params.id, function (err, order) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        } else {
            res.json({
                status: "success",
                message: "Order retrieved successfully",
                data: order
            });
        }
    });
};

exports.index = function (req, res) {
    Orders.find({order_resto_id: req.params.id}, function (err, orders) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        } else {
            res.json({
                status: "success",
                message: "Orders retrieved successfully",
                data: orders.reverse()
            });
        }
    });
};