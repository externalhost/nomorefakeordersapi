const jwt = require('jsonwebtoken');

exports.verify = function (req, res, next) {
  let token = req.headers['token'];
  let msg = {auth: false, message: 'No token provided.'};
  if (!token) res.status(500).send(msg);
  jwt.verify(token, 'SECRET_KEY', function (err, decoded) {
      let msg = {auth: false, message: 'Failed to authenticate token.'};
      if (err) {console.log(err); res.status(500).send(msg);}
      next();
  });
}