const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.index = function (req, res) {
    User.get(function (err, users) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        } else {
            res.json({
                status: "success",
                message: "Users retrieved successfully",
                data: users
            });
        }
    });
};

exports.new = function (req, res) {
    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            res.send(err);
        }
        if (user) {
            res.json({
                message: 'User existing',
                data: user
            })
        } else {
            let user = new User();
            user.firstname = req.body.firstname;
            user.lastname = req.body.lastname;
            user.adresse = req.body.adresse;
            user.restoname = req.body.restoname;
            user.siret = req.body.siret;
            user.email = req.body.email;
            user.phone = req.body.phone;
            
            bcrypt.genSalt(10, function (err, salt) {
                bcrypt.hash(req.body.password, salt, function (err, pwcrypt) {
                    user.password = pwcrypt;
                    user.save(function (err) {
                        if (err) {
                            res.json(err);
                        } else {
                            res.json({
                                message: 'New user created!',
                                data: user
                            });
                        }
                    });
                })
            })
        }
    });
};

exports.view = function (req, res) {
    User.findById(req.params.user_id, function (err, user) {
        if (err) {
            res.send(err);
        } else {
            res.json({
                message: 'User details loading..',
                data: user
            });
        }
    });
};

exports.login = function (req, res) {
    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) {
            res.status(403).send({
                message: "user not found"
            });
        } else {
            if (user){
                bcrypt.compare(req.body.password, user.password).then((data) => {
                    console.log(data);
                    res.json({
                        message: 'Login details loading..',
                        token: jwt.sign({
                            data: {
                                user_id: user._id,
                                role: user.is_admin
                            }
                        }, 'SECRET_KEY', { expiresIn: 604800 }),
                        data: user
                    });
                });
            } else {
                res.json({
                    message: "Password doesn't match",
                });
            } 
        }
    });
};

exports.update = function (req, res) {
    User.findById(req.params.user_id, function (err, user) {
        if (err) {
            res.send(err);
        } else {
            user.firstname = req.body.firstname;
            user.lastname = req.body.lastname;
            user.adresse = req.body.adresse;
            user.restoname = req.body.restoname;
            user.siret = req.body.siret;
            user.email = req.body.email;
            user.phone = req.body.phone;

            user.save(function (err) {
            if (err) {
                res.json(err);
            } else {
                res.json({
                    message: 'User Info updated',
                    data: user
                });
            }
            });
        }
    });
};


exports.delete = function (req, res) {
    User.remove({
        _id: req.params.user_id
    }, function (err, user) {
        if (err) {
            res.send(err);
        } else {
            res.json({
                status: "success",
                message: 'User deleted'
            });
        }
    });
};